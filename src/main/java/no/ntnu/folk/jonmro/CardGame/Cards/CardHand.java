package no.ntnu.folk.jonmro.CardGame.Cards;


import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class Containing a Collcection of PlayingCard and checking what the Collcection contains
 */
public class CardHand {

    /**
     * Collection so that various collection subclasses can be utilized
     */
    private final Collection<PlayingCard> hand;
    private int numCards;
    /**
     * Construct hand given cards
     * @param cards
     */
    public CardHand(Collection<PlayingCard> cards) {
        this.hand = cards;
        this.numCards = cards.size();
    }

    /**
     * @return sum of face of all cards in hand
     */
    public int getSumOfFace(){
        return this.hand.stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * @return All hearts in hand in new List
     */
    public List<PlayingCard> getSuitHearts() {
        return this.hand.stream().filter(card -> card.getSuit() == 'H').collect(Collectors.toList());
    }

    /**
     * @return if hand contains Queen of spades
     */
    public Boolean HasQueenOfSpade() {
        return this.hand.stream().anyMatch(card -> card.getAsString().trim().equals("S12"));
    }

    /**
     * @return if hand is built up of only one Suit of card
     */
    public Boolean isFlush(){
        return this.hand.stream().allMatch(card -> card.getSuit() == 'H') ||
                this.hand.stream().allMatch(card -> card.getSuit() == 'S') ||
                this.hand.stream().allMatch(card -> card.getSuit() == 'C') ||
                this.hand.stream().allMatch(card -> card.getSuit() == 'D');

    }

    /**
     * @return hand in form of String [H1, S5, H3, ...]
     */
    @Override
    public String toString() {
        return this.hand.stream().map(card -> card.getAsString()).collect(Collectors.toList()).toString();
    }

    /**
     * @return get numbers of cards in hand
     */
    public int getNumCards() {
        return this.numCards;
    }
}
