package no.ntnu.folk.jonmro.CardGame.Cards;

import java.util.*;

/**
 * Class containing List of PlayingCard representing standard 52 deck of cards
 */
public class DeckOfCards {
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private final List<PlayingCard> deck;
    private int cardsDrawn = 0;

    /**
     * Construct 52 PlayingCard deck with 4 suits and 13 faces for each suit in non-random order
     */
    public DeckOfCards() {
        var deck = new ArrayList<PlayingCard>();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 13; j++) {
                deck.add(new PlayingCard(suit[i],j));
            }
        }
        this.deck = deck;
    }

    /**
     * Construct deck based of List of PlayingCard
     * @param deck
     */
    public DeckOfCards(List<PlayingCard> deck) {
        this.deck = deck;
    }

    /**
     * @param n number of cards in new hand
     * @return new hand drawn from deck
     */
    public CardHand dealHand(int n) {
        return new CardHand(drawCards(n));
    }

    /**
     * Randomize deck
     */
    public void shuffleDeck() {
        Collections.shuffle(this.deck);
    }


    /**
     * @param n number of cards to draw
     * @return subList of deck equal in length to n
     */
    public List<PlayingCard> drawCards(int n) {
        int Drawn = cardsDrawn;
        if (cardsDrawn >= 52 || (cardsDrawn+n) > 52) {
            shuffleDeck();
            cardsDrawn = 0;
            Drawn = cardsDrawn;
        }
        cardsDrawn += n;
        return deck.subList(Drawn, n+Drawn);
    }


    /**
     * @return deck
     */
    public List<PlayingCard> getDeck() {
        return this.deck;
    }
}
