package no.ntnu.folk.jonmro.CardGame;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import no.ntnu.folk.jonmro.CardGame.Cards.CardHand;
import no.ntnu.folk.jonmro.CardGame.Cards.DeckOfCards;
import no.ntnu.folk.jonmro.CardGame.Cards.PlayingCard;

import java.util.stream.Collectors;

/**
 * Controller for JavaFX GUI
 * Responding to user input in GUI
 */
public class AppController {
    private final DeckOfCards deck = new DeckOfCards();
    {this.deck.shuffleDeck();}
    private CardHand hand = this.deck.dealHand(5);
    /**
     * Button element used to show if hand contains Queen of Spades, Just for esthetics. Not an actual button.
     */
    @FXML
    private Button hasQueenOfSpades;

    /**
     * Button element used to show if hand is flush, Just for esthetics. Not an actual button.
     */
    @FXML
    private Button isFlush;

    /**
     * Button element used to show hearts in hand, just for esthetics. Not an actual button.
     */
    @FXML
    private Button numHearts;

    /**
     * Button element used to show sum, Just for esthetics. Not an actual button.
     */
    @FXML
    private Button sum;

    /**
     * Showing all cards in hand
     */
    @FXML
    private TextArea textArea;


    /**
     * EventHandler for deal button on the GUI
     * deals new hand and resets gui elements
     */
    @FXML
    public void handleButtondeal() {
        this.hand = this.deck.dealHand(5);
        textArea.clear();
        textArea.setText(this.hand.toString());
        sum.setText("");
        numHearts.setText("");
        isFlush.setText("");
        hasQueenOfSpades.setText("");
    }

    /**
     * EventHandler for check button on the GUI
     * Performs check on hand to find out its attributes and updates GUI accordingly
     */
    @FXML
    public void handleButtoncheck() {
        sum.setText(String.valueOf(this.hand.getSumOfFace()));
        numHearts.setText(this.hand.getSuitHearts().stream().map(PlayingCard::getAsString).collect(Collectors.toList()).toString());
        if (this.hand.isFlush()) isFlush.setText("Yes");
        else isFlush.setText("No");
        if (this.hand.HasQueenOfSpade()) hasQueenOfSpades.setText("Yes");
        else hasQueenOfSpades.setText("No");
    }

}
