module no.ntnu.folk.jonmro {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.folk.jonmro.CardGame to javafx.fxml;
    exports no.ntnu.folk.jonmro.CardGame;
    exports no.ntnu.folk.jonmro.CardGame.Cards;
    opens no.ntnu.folk.jonmro.CardGame.Cards to javafx.fxml;

}
