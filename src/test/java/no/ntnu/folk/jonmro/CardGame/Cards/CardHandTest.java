package no.ntnu.folk.jonmro.CardGame.Cards;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CardHandTest {

    @Test
    void getSumOfFace() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('S', 2));
        cards.add(new PlayingCard('H', 3));
        cards.add(new PlayingCard('C', 4));
        CardHand hand = new CardHand(cards);
        assertEquals(10,hand.getSumOfFace());
    }

    @Test
    void getSuitHearts() {
        List<PlayingCard> cards = new ArrayList<>();
        List<PlayingCard> hearts = new ArrayList<>();
        hearts.add(new PlayingCard('H', 1));
        hearts.add(new PlayingCard('H', 3));
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('S', 2));
        cards.add(new PlayingCard('H', 3));
        cards.add(new PlayingCard('C', 4));
        CardHand hand = new CardHand(cards);
        assertEquals(hearts.get(1).getAsString(),hand.getSuitHearts().get(1).getAsString());
        assertEquals(hearts.get(0).getAsString(),hand.getSuitHearts().get(0).getAsString());
    }

    @Test
    void hasQueenOfSpade() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('S', 12));
        cards.add(new PlayingCard('H', 3));
        cards.add(new PlayingCard('C', 4));
        CardHand hand = new CardHand(cards);
        assertTrue(hand.HasQueenOfSpade());
    }

    @Test
    void isFlush() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('S', 12));
        cards.add(new PlayingCard('H', 3));
        cards.add(new PlayingCard('C', 4));
        CardHand hand = new CardHand(cards);
        assertFalse(hand.isFlush());
        cards.clear();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('H', 2));
        cards.add(new PlayingCard('H', 6));
        hand = new CardHand(cards);
        assertTrue(hand.isFlush());


    }

    @Test
    void testToString() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('S', 12));
        cards.add(new PlayingCard('H', 3));
        cards.add(new PlayingCard('C', 4));
        CardHand hand = new CardHand(cards);
        assertEquals("[H1, S12, H3, C4]", hand.toString());
    }
}