package no.ntnu.folk.jonmro.CardGame.Cards;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
    @Test
    void ammountOfCards() {
        DeckOfCards deck = new DeckOfCards();
        assertEquals(52, deck.getDeck().size());
    }

    @Test
    void dealHand() {
        DeckOfCards deck = new DeckOfCards();
        assertNotEquals(deck.dealHand(5), deck.dealHand(5));
        assertEquals(deck.dealHand(5).getNumCards(), 5);
    }

    @Test
    void shuffleDeck() {
        ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S',0));
        cards.add(new PlayingCard('S',1));
        cards.add(new PlayingCard('S',2));
        cards.add(new PlayingCard('S',3));
        DeckOfCards deck = new DeckOfCards();
        assertEquals(deck.getDeck().get(0).getAsString(), cards.get(0).getAsString());
        assertEquals(deck.getDeck().get(1).getAsString(), cards.get(1).getAsString());
        assertEquals(deck.getDeck().get(2).getAsString(), cards.get(2).getAsString());
        assertEquals(deck.getDeck().get(3).getAsString(), cards.get(3).getAsString());
        deck.shuffleDeck();
        assertNotEquals(deck.getDeck().get(0).getAsString(), cards.get(0).getAsString());
        assertNotEquals(deck.getDeck().get(1).getAsString(), cards.get(1).getAsString());
        assertNotEquals(deck.getDeck().get(2).getAsString(), cards.get(2).getAsString());
        assertNotEquals(deck.getDeck().get(3).getAsString(), cards.get(3).getAsString());
    }

}